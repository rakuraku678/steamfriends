package notifiers;

import models.GameDiscount;
import models.UserDiscount;
import play.mvc.Mailer;

public class Mails extends Mailer {

	public static void sendDiscountEmail(UserDiscount user, int gameLimitPrice, String gameName, float currentPrice) {
		setFrom("info@oursharedsteamgames.com");
		setSubject("It´s time to buy a game!");
		addRecipient(user.email);
		send(gameLimitPrice, gameName, currentPrice);
	}
}