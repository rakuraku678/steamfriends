package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import play.libs.F.Promise;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Controller;
import utils.JsonUtils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ApiCall extends Controller {

	public static void index() {
		render();
	}

	public static void steamBot() throws IOException, InterruptedException, ExecutionException {
		
		
		String parametro =  params.get("text");
		String texto = parametro.substring(parametro.indexOf("steam")+6,parametro.length());
		
		String rta = getSharedGamesForApi(texto);

		Map<String,String> mapa = Maps.newHashMap();
		mapa.put("text", rta);
		renderJSON(mapa);
		
			//PARA VERIFICAR TOKEN FACEBOOK NO BORRAR
//		String verifyToken = params.get("hub.verify_token");
//		
//		if (verifyToken.equals("Childe_Roland")){
//			renderText(params.get("hub.challenge"));
//		}
//		else {
//			renderText("nolas");
//		}
		
	}

	
	public static void telegramBot(String body) throws IOException, InterruptedException, ExecutionException {
		JSONObject json = new JSONObject(body);
		String mensaje = json.getJSONObject("message").toString();
		
		String texto = json.getJSONObject("message").getString("text");
		int chatId = json.getJSONObject("message").getJSONObject("chat").getInt("id");
		
		String rta = getSharedGamesForApi(texto);

		WS.WSRequest req = WS.url("https://api.telegram.org/bot230508201:AAE-Mqg9Hf04eJtDTL-AOxIoXYDUidcBl-M/sendMessage");
		
		Map<String, String> headers = Maps.newHashMap();
		headers.put("Content-Type", "application/json");
		req.headers(headers);
		
		Map bodyReq = Maps.newHashMap();
		bodyReq.put("chat_id", chatId);
		bodyReq.put("text", rta);
		String jsonReq = new Gson().toJson(bodyReq);
		req.body(jsonReq);
		
		HttpResponse st= req.post();
		
	}
	
	
	public static List<String> getIdPlayers(List<String> aliases) throws IOException {


		List<Promise<HttpResponse>> promisesList = Lists.newArrayList();

		for (String alias : aliases) {

			Promise<HttpResponse> promise = WS
					.url("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?vanityurl=" + alias.trim()
							+ "&key=99D76FF19C90C360AB9F7A3345FFCECE")
					.getAsync();

			promisesList.add(promise);

		}

		Promise<List<WS.HttpResponse>> promises = Promise.waitAll(promisesList);

		await(promises);

		List<String> ids = Lists.newArrayList();
		for (Promise<HttpResponse> promise : promisesList) {
			try {
				String id = promise.get().getJson().getAsJsonObject().get("response").getAsJsonObject().get("steamid")
						.getAsString();
				ids.add(id);
			} catch (Exception e) {
				renderText("error");
			}
		}

		return ids;

	}
	
	public static String getSharedGamesForApi(String usersIds) throws IOException {

		String[] idsArr = usersIds.split(",");
		
		if (idsArr.length<=10){
			
			List<String> aliases = Lists.newArrayList();
			List<String> totalIds = Lists.newArrayList();
			
			for (String id : idsArr) {
				if (!StringUtils.isNumeric(id) || id.length()!=17){
					aliases.add(id);
				}
				else {
					totalIds.add(id);
				}
			}
			
			List<String> idsFromAlias = getIdPlayers(aliases);
			totalIds.addAll(idsFromAlias);
			
		
			List<Promise<HttpResponse>> promisesList = Lists.newArrayList();
			
			for (String id : totalIds) {
	
				Promise<HttpResponse> promise = getUserGamesPromise(id);
				promisesList.add(promise);
				
			}
			
			Promise<List<WS.HttpResponse>> promises = Promise.waitAll(promisesList);
	
			await(promises);
			
			List<List<Integer>> usersGameLists = Lists.newArrayList();
			for (Promise<HttpResponse> promise : promisesList) {
				try {
					List<Integer> user1GameList = parseGameListPromise(promise);
					usersGameLists.add(user1GameList);
				} catch (Exception e) {
					return "error";
				}
			}
	
			List<Integer> sharedGames = superIntersection(usersGameLists);
	
			List<String> titlesCsv = Lists.newArrayList();
			if (!sharedGames.isEmpty()){
				titlesCsv = getTitles(sharedGames);	
			}
			
			String response  = "";
			for (String string : titlesCsv) {
				response = response + string + "\n";
			}
			return response;
		}
		return "too much players";
	}
	
	public static List<Integer> superIntersection(List<List<Integer>> usersGameLists) {
		List<Integer> sharedElements = Lists.newArrayList();
		try {
			sharedElements = ListUtils.intersection(usersGameLists.get(0), usersGameLists.get(1));
		} catch (Exception e) {
			return sharedElements;
		}

		if (usersGameLists.size() > 2) {
			for (int i = 3; i <= usersGameLists.size(); i++) {
				sharedElements = ListUtils.intersection(sharedElements, usersGameLists.get(i - 1));
			}
		}
		return sharedElements;
	}

	private static List<Integer> parseGameListPromise(Promise<HttpResponse> promise1)
			throws InterruptedException, ExecutionException {
		HttpResponse resp = promise1.get();
		List<Integer> user1GameList = Lists.newArrayList();
		try {
			JsonArray jArray = resp.getJson().getAsJsonObject().get("response").getAsJsonObject().get("games")
					.getAsJsonArray();

			int appId;
			for (JsonElement jsonElement : jArray) {
				appId = jsonElement.getAsJsonObject().get("appid").getAsInt();
				if (appId != 0) {
					user1GameList.add(appId);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return user1GameList;
	}

	private static Promise<HttpResponse> getUserGamesPromise(String user1Id) {
		Promise<HttpResponse> promise1 = WS
				.url("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=99D76FF19C90C360AB9F7A3345FFCECE&steamid="
						+ user1Id + "&format=json")
				.getAsync();
		return promise1;
	}

	private static List<String> getTitles(List<Integer> listaAppIds) {
		List<String> csvList = Lists.newArrayList();

		JsonObject o, o1, data;
		String name = "";
		String success = "";
		HttpResponse respo;

		List<Promise<HttpResponse>> listPromise = Lists.newArrayList();
		Promise<WS.HttpResponse> r;
		String a = "";
		String gameCsv = "";
		for (Integer appId : listaAppIds) {
			// gameCsv = Cache.get("game_" + appId, String.class);

			if (Strings.isNullOrEmpty(gameCsv)) {
				r = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + appId).getAsync();
				listPromise.add(r);
			} else {
				csvList.add(gameCsv);
			}
		}

		Promise<List<WS.HttpResponse>> promises = Promise.waitAll(listPromise);
		await(promises);

		String appId = "";
		for (Promise<HttpResponse> promise : listPromise) {
			try {
				respo = promise.get();
				JsonElement jsonResp = respo.getJson();

				a = jsonResp.toString();

				appId = a.substring(2, a.indexOf("\":{"));

				o = jsonResp.getAsJsonObject();
				o1 = JsonUtils.getJsonObjectFromJson(o, appId + "").getAsJsonObject();

				success = JsonUtils.getStringFromJson(o1, "success");

				if ("true".equals(success)) {
					data = JsonUtils.getJsonObjectFromJson(o1, "data").getAsJsonObject();
					name = JsonUtils.getStringFromJson(data, "name");
					csvList.add(name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return csvList;
	}

}