package controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Strings;

import models.GameDiscount;
import models.UserDiscount;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.modules.morphia.Model.MorphiaQuery;
import play.mvc.Controller;

public class DiscountController extends Controller {

	public static void index() {
		String email = session.get("userEmail");
		UserDiscount user = UserDiscount.findByEmail(email);
		List<GameDiscount> gameList = GameDiscount.findByUser(user);
		render(gameList);
	}

	public static void searchGames(String text) {
		HttpResponse response = WS
				.url("http://store.steampowered.com/search/suggest?term=" + text + "&f=games&cc=AR&l=spanish&v=1945604")
				.get();
		String responseString = response.getString();
		renderText(responseString);
	}

	public static void saveGamesDiscount(String email, String game, String price) {
		UserDiscount user = UserDiscount.findByEmail(email);
		String cookieEmail = session.get("userEmail");
		if (Strings.isNullOrEmpty(cookieEmail)) {
			session.put("userEmail", user.email);
		}

		if (user == null) {
			user = UserDiscount.createUserDiscount(email, true);
		}
		price = formatPrice(price);
		GameDiscount gd = GameDiscount.createOrUpdateGameDiscuont(game, Integer.parseInt(price), user);
		render(gd);
	}

	public static void removeGamesDiscount(String gameId) {
		String userEmail = session.get("userEmail");
		boolean success = GameDiscount.removeGame(gameId, userEmail);
		renderText(success);
	}

	private static String formatPrice(String price) {
		if (price.length() == 1) {
			price = "0" + price + "00";
		}
		if (price.contains(",") || price.contains(".")) {
			if (price.length() == 3) {
				price = StringUtils.remove(price, ".");
				price = StringUtils.remove(price, ",");
				price = "0" + price + "0";
			} else if (price.length() == 4) {
				String[] arr = price.split("\\,");
				if (arr != null && arr.length > 1) {
					if (price.split("\\,")[0].length() == 1) {
						price = StringUtils.remove(price, ",");
						price = "0" + price;
					} else {
						price = StringUtils.remove(price, ",");
						price = price + "0";
					}
				} else {
					arr = price.split("\\.");
					if (arr != null && arr.length > 0) {
						if (price.split("\\.")[0].length() == 1) {
							price = StringUtils.remove(price, ".");
							price = "0" + price;
						} else {
							price = StringUtils.remove(price, ".");
							price = price + "0";
						}
					}
				}
			} else if (price.length() == 5) {
				price = StringUtils.remove(price, ",");
				price = StringUtils.remove(price, ".");
			}
		} else {
			while (price.length() < 4) {
				price += "0";
			}
		}
		return price;
	}

	public static void renderSuccess() {
		render();
	}

}