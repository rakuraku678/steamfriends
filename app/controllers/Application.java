package controllers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import dtos.GameDto;
import dtos.PlayerDto;
import models.Game;
import play.libs.F.Promise;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Controller;
import utils.JsonUtils;

public class Application extends Controller {

	private static final String KEY = "99D76FF19C90C360AB9F7A3345FFCECE";

	public static void index() {
		render();
	}

	public static void shareLink(String usersIds) {
		render("Application/index.html", usersIds);
	}

	public static void getFriends(String userId) {
		String playerId = getPlayerId(userId);
		List<PlayerDto> listDto = Lists.newArrayList();
		String friendIdCsv = "";
		HttpResponse respo = WS
				.url("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=" + KEY + "&steamid=" + playerId)
				.get();
		JsonElement list = JsonUtils.getJsonObjectFromJson(respo.getJson().getAsJsonObject(), "friendslist");
		if (list != null) {
			JsonArray arr = JsonUtils.getJsonArrayFromJson(list.getAsJsonObject(), "friends").getAsJsonArray();
			for (JsonElement jsonElement : arr) {
				friendIdCsv += jsonElement.getAsJsonObject().get("steamid").getAsString() + ",";
			}
			listDto = getPlayersInfo(friendIdCsv);
		}
		render(listDto);
	}

	public static List<PlayerDto> getPlayersInfo(String playersIdCsv) {
		List<PlayerDto> dtoList = Lists.newArrayList();
		HttpResponse respo = WS.url("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + KEY
				+ "&steamids=" + playersIdCsv).get();
		JsonElement respoJson = JsonUtils.getJsonObjectFromJson(respo.getJson().getAsJsonObject(), "response");
		if (respoJson != null) {
			JsonArray arr = JsonUtils.getJsonArrayFromJson(respoJson.getAsJsonObject(), "players");
			PlayerDto dto;
			for (JsonElement jsonElement : arr) {
				dto = new PlayerDto();
				dto.id = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "steamid");
				dto.nickName = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "personaname");
				dto.profileUrl = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "profileurl");
				dto.avatar = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "avatar");
				dto.avatarMedium = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "avatarmedium");
				dto.avatarFull = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "avatarfull");
				dto.realName = JsonUtils.getStringFromJson(jsonElement.getAsJsonObject(), "realname");
				dtoList.add(dto);
			}

		}

		return dtoList;
	}

	private static String getPlayerId(String alias) {
		HttpResponse respo = WS.url("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?vanityurl="
				+ alias.trim() + "&key=" + KEY + "").get();
		return respo.getJson().getAsJsonObject().get("response").getAsJsonObject().get("steamid").getAsString();
	}

	public static List<String> getIdPlayers(List<String> aliases) throws IOException {

		List<Promise<HttpResponse>> promisesList = Lists.newArrayList();
		Promise<HttpResponse> promise;
		for (String alias : aliases) {
			promise = WS.url("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?vanityurl=" + alias.trim()
					+ "&key=" + KEY + "").getAsync();

			promisesList.add(promise);
		}

		Promise<List<WS.HttpResponse>> promises = Promise.waitAll(promisesList);

		await(promises);

		List<String> ids = Lists.newArrayList();
		for (Promise<HttpResponse> promiseR : promisesList) {
			try {
				ids.add(promiseR.get().getJson().getAsJsonObject().get("response").getAsJsonObject().get("steamid")
						.getAsString());
			} catch (Exception e) {
				e.printStackTrace();
				renderText("error");
			}
		}

		return ids;

	}

	public static void getSharedGames(String usersIds) throws IOException, InterruptedException, ExecutionException {
		String[] idsArr = usersIds.split(",");

		if (idsArr.length <= 10) {

			List<String> aliases = Lists.newArrayList();
			List<String> totalIds = Lists.newArrayList();

			for (String id : idsArr) {
				if (!StringUtils.isNumeric(id) || id.length() != 17) {
					aliases.add(id);
				} else {
					totalIds.add(id);
				}
			}

			List<String> idsFromAlias = getIdPlayers(aliases);
			totalIds.addAll(idsFromAlias);

			List<Promise<HttpResponse>> promisesList = Lists.newArrayList();
			Promise<HttpResponse> promise;
			for (String id : totalIds) {
				promise = getUserGamesPromise(id);
				promisesList.add(promise);
			}

			Promise<List<WS.HttpResponse>> promises = Promise.waitAll(promisesList);

			await(promises);

			List<List<Integer>> usersGameLists = Lists.newArrayList();
			List<Integer> user1GameList;
			for (Promise<HttpResponse> promiseR : promisesList) {
				try {
					user1GameList = parseGameListPromise(promiseR);
					usersGameLists.add(user1GameList);
				} catch (Exception e) {
					e.printStackTrace();
					renderText("error");
				}
			}

			List<Integer> sharedGames = superIntersection(usersGameLists);

			List<GameDto> gameDtoList = Lists.newArrayList();
			if (!sharedGames.isEmpty()) {
				gameDtoList = getTitles(sharedGames);
			}
			render(gameDtoList, usersIds);
		}
	}

	public static List<Integer> superIntersection(List<List<Integer>> usersGameLists) {
		List<Integer> sharedElements = Lists.newArrayList();
		try {
			sharedElements = ListUtils.intersection(usersGameLists.get(0), usersGameLists.get(1));
		} catch (Exception e) {
			e.printStackTrace();
			return sharedElements;
		}

		if (usersGameLists.size() > 2) {
			for (int i = 3; i <= usersGameLists.size(); i++) {
				sharedElements = ListUtils.intersection(sharedElements, usersGameLists.get(i - 1));
			}
		}
		return sharedElements;
	}

	private static List<Integer> parseGameListPromise(Promise<HttpResponse> promise1)
			throws InterruptedException, ExecutionException {
		HttpResponse resp = promise1.get();
		List<Integer> user1GameList = Lists.newArrayList();
		try {
			JsonArray jArray = resp.getJson().getAsJsonObject().get("response").getAsJsonObject().get("games")
					.getAsJsonArray();

			int appId;
			for (JsonElement jsonElement : jArray) {
				appId = jsonElement.getAsJsonObject().get("appid").getAsInt();
				if (appId != 0) {
					user1GameList.add(appId);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return user1GameList;
	}

	private static Promise<HttpResponse> getUserGamesPromise(String user1Id) {
		Promise<HttpResponse> promise1 = WS.url("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key="
				+ KEY + "&steamid=" + user1Id + "&format=json").getAsync();
		return promise1;
	}

	private static List<GameDto> getTitles(List<Integer> listaAppIds) {
		List<GameDto> dtoList = Lists.newArrayList();
		HttpResponse respo;

		List<Promise<HttpResponse>> listPromise = Lists.newArrayList();
		Promise<WS.HttpResponse> r;
		Game game;
		for (Integer appId : listaAppIds) {
			game = Game.findByGameId(appId.toString());
			if (game == null) {
				r = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + appId).getAsync();
				listPromise.add(r);
			} else {
				dtoList.add(Game.toDto(game));
			}
		}

		Promise<List<WS.HttpResponse>> promises = Promise.waitAll(listPromise);
		await(promises);

		for (Promise<HttpResponse> promise : listPromise) {
			try {
				respo = promise.get();
				dtoList.add(Game.createGameDto(respo));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return dtoList;
	}

}