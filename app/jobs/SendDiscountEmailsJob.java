package jobs;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import models.Game;
import models.GameDiscount;
import notifiers.Mails;
import play.jobs.Every;
import play.jobs.Job;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.modules.morphia.Model.MorphiaQuery;
import utils.JsonUtils;

@Every("180s")
public class SendDiscountEmailsJob extends Job {

	@Override
	public void doJob() {
		System.out.println("Running discount job");
		float price;
		MorphiaQuery q = GameDiscount.q();
		q.or(q.criteria("emailSent").equal("false"), q.criteria("emailSent").doesNotExist());
		List<GameDiscount> gameList = q.fetchAll();
		List<GameDiscount> gameNotified = Lists.newArrayList();
		Game realGame;
		for (GameDiscount game : gameList) {
			price = getGamePrice(game.gameId);
			realGame = Game.findByGameId(game.gameId);
			realGame.price = price;
			realGame.save();
			if (price < Float.parseFloat(Integer.toString(game.limitPrice))) {
				Mails.sendDiscountEmail(game.user, game.limitPrice, getGameName(game.gameId), price);
				gameNotified.add(game);
			}
		}

		// Despues de mandarle el mail, lo borro asi no le vuelve a mandar
		for (GameDiscount game : gameNotified) {
			game.emailSent = true;
			game.save();
		}
		System.out.println("job ended");
	}

	private float getGamePrice(String gameId) {
		float price = 999999f;
		try {
			HttpResponse respo = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + gameId).get();
			JsonObject o = respo.getJson().getAsJsonObject();
			JsonObject o1 = JsonUtils.getJsonObjectFromJson(o, gameId).getAsJsonObject();
			JsonObject data = JsonUtils.getJsonObjectFromJson(o1, "data").getAsJsonObject();
			JsonObject prices = JsonUtils.getJsonObjectFromJson(data, "price_overview").getAsJsonObject();
			if (prices != null) {
				String finalPrice = JsonUtils.getStringFromJson(prices, "final");
				finalPrice = finalPrice.replaceAll(",", ".");
			}
			// Ver el else de este caso que juegos no tienen precio.
		} catch (Exception e) {
			System.out
					.println("Error obteniendo precio: http://store.steampowered.com/api/appdetails/?appids=" + gameId);
			e.printStackTrace();
		}

		return price;
	}

	private String getGameName(String gameId) {
		HttpResponse r = WS.url("http://store.steampowered.com/api/appdetails/?appids=" + gameId).get();
		JsonElement e = r.getJson().getAsJsonObject();
		e = JsonUtils.getJsonObjectFromJson(e.getAsJsonObject(), gameId);
		e = JsonUtils.getJsonObjectFromJson(e.getAsJsonObject(), "data");
		String name = JsonUtils.getStringFromJson(e.getAsJsonObject(), "name");
		return name;
	}
}
