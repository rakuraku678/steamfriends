package models;

import java.util.List;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;

import play.modules.morphia.Model;

@Entity
public class GameDiscount extends Model {

	public String gameId;
	public int limitPrice;

	@Reference
	public UserDiscount user;

	@Reference
	public Game game;
	public boolean emailSent = false;

	public static GameDiscount createOrUpdateGameDiscuont(String gameId, int price, UserDiscount user) {

		GameDiscount gd = GameDiscount.getByGameId(gameId, user);
		boolean returnGd = true;
		if (gd == null) {
			gd = new GameDiscount();
			gd.gameId = gameId;
			gd.limitPrice = price;
			gd.user = user;
		} else {
			gd.limitPrice = price;
			returnGd = false;
		}
		gd.emailSent = false;

		Game game = Game.findByGameId(gameId);
		if (game == null) {
			gd.game = Game.createGame(gameId);
		}

		gd.save();

		return returnGd ? gd : null;
	}

	private static GameDiscount getByGameId(String gameId, UserDiscount user) {
		MorphiaQuery q = GameDiscount.q();
		q.field("gameId").equal(gameId);
		q.field("user").equal(user);
		q.field("emailSent").equal(false);
		return q.first();
	}

	public Game getGame() {
		if (this.game == null) {
			this.game = Game.findByGameId(this.gameId);
		}
		return this.game;
	}

	public String getGameName() {
		if (this.getGame() != null) {
			return this.getGame().gameName;
		} else {
			return this.gameId;
		}
	}

	public String getLimitPrice() {
		String price = this.limitPrice + "";
		if (price.length() == 4) {
			price = price.substring(0, 2) + "." + price.substring(2, 4);
		} else if (price.length() == 3) {
			price = price.substring(0, 1) + "." + price.substring(1, 3);
		}
		return price;
	}

	public static List<GameDiscount> findByUser(UserDiscount user) {
		MorphiaQuery q = GameDiscount.q();
		q.field("user").equal(user);
		q.field("emailSent").equal(false);
		return q.fetchAll();
	}

	public static boolean removeGame(String gameId, String userEmail) {
		UserDiscount user = UserDiscount.findByEmail(userEmail);
		if(user != null){
			MorphiaQuery q = GameDiscount.q();
			q.field("user").equal(user);
			q.field("gameId").equal(gameId);
			q.or(q.criteria("emailSent").equal(false), q.criteria("emailSent").doesNotExist());
			GameDiscount gameToBeRomved = q.first();
			if(gameToBeRomved != null){
				gameToBeRomved.delete();
				return true;
			}
		}
		return false;
	}
}
